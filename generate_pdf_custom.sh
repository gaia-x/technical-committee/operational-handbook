set -ex

docs/mission_documents/build_md_pdf.sh

pdf=pdf/*

for file in $pdf; do
    filename=$(basename $file)
    echo $filename
    curl -u $DCP_USER:$DCP_PWD -T $file https://community.gaia-x.eu/remote.php/dav/files/GAIAX/Mission%20Documents%20-%20Groups/$filename
done