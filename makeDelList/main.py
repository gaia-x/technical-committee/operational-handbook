import gitlab
import yaml

gl = gitlab.Gitlab()

projects = yaml.safe_load(open("makeDelList/config.yml", "r"))

for project_name in projects:
    # print(projects[project_name])

    project_id = projects[project_name]
    project = gl.projects.get(project_id)

    namespace = project.path_with_namespace.split('/')
    namespace = namespace[1:]
    namespace.insert(0, 'https://gaia-x.gitlab.io')

    s = "/"
    url = s.join(namespace)

    print(url)

    with open('makeDelList/deliverables_list.md', 'a') as f:
        f.write(f"- {project.name} : [{url}]({url})\n")

    with open('makeDelList/deliverables_links.txt', 'a') as f:
        f.write(f"<p><a href='{url}'>{url}</a></p>\n")

        
